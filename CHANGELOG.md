# Changelog MediaDisplay-Server (Liedanzeige)

## [1.2.2] - 2020-04-01

### Hinzugefügt
- Modus für Studiouhr

## [1.1.1] - 2019-05-08

### Geändert
- Stabilität und Leistung verbessert

### Hinzugefügt
- Automatischer Neustart jede Nacht
- Diagnoseinformationen werden geloggt

## [1.1.0] - 2019-01-31

### Hinzugefügt
- DisplayServer um Abfragen für Saalanzeige erweitert

## [1.0.0] - 2018-03-07

### Hinzugefügt
- Leerer Bildschirm auf Seite Idle
- DisplayServer erweitert und Style geändert
- Updates können gemacht werden
- Nachrichten auf Startbildschim
- Beide Adressen für BME 280 werden ausprobiert

## [0.5.0] - 2017-07-08

### Hinzugefügt
- DisplayServer erweitert
- BME280 eingebunden
- LED-Leiste für Tontechniker eingebunden
- Lizenzen hinzugefügt
- Allgemeine Liederbücher können angegeben werden
- Reihenfolge der Solisten kann geändert werden

## [0.4.0] - 2017-04-24

### Hinzugefügt
- QR-Code für IP-Adresse
- LED blinkt im Standby
- Seite mit Systeminformationen

## [0.3.0] - 2017-04-19

### Hinzugefügt
- DisplayServer implementiert
- Uhranzeigen und Übergangszeiten einstellbar gemacht

## [0.2.0]

### Hinzugefügt
- Uhrzeit und Datum über GUI einstellbar

## [0.1.1]

### Hinzugefügt
- Initiale Version
