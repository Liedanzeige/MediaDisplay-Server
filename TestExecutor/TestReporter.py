from datetime import datetime


class TestReporter(object):

    def __init__(self, test_results, start, duration):
        self.test_results = test_results
        self.start = start
        self.duration = duration

    def passed(self):
        if not self.test_results:
            return False
        return all([i.verdict is True for i in self.test_results])

    def executed(self):
        if not self.test_results:
            return False
        return all([i.verdict is not None for i in self.test_results])

    def get_html_report(self):
        html = u"<!DOCTYPE html>\n"
        html += u"<html>\n"
        html += u"  <head>\n"
        html += u"    <title>Liedanzeige Test Result {0}</title>\n".format(self.start.strftime('%Y.%m.%d %H:%M:%S'))
        html += u"  </head>\n"
        html += u"  <body>\n"
        html += u"    <h1>Liedanzeige Test Result</h1>\n"
        if not self.executed():
            html += u'	<div style="background-color: lightgray; color: gray; font-size: 60px; ' \
                    u'padding: 10px; margin: 5px; display: inline-block;">No Result</div>\n'
        elif self.passed():
            html += u'	<div style="background-color: lightgreen; color: green; font-size: 60px; ' \
                    u'padding: 10px; margin: 5px; display: inline-block;">Pass</div>\n'
        else:
            html += u'	<div style="background-color: tomato; color: darkred; font-size: 60px; ' \
                    u'padding: 10px; margin: 5px; display: inline-block;">Fail</div>\n'
        html += u'	<table>\n'
        html += u'		<tr>\n'
        html += u'			<td style="text-align: right;"><b>Start</b></td>\n'
        html += u'			<td>{0}</td>\n'.format(self.start.isoformat())
        html += u'		</tr>\n'
        html += u'		<tr>\n'
        html += u'			<td style="text-align: right;"><b>Duration</b></td>\n'
        html += u'			<td>{0}s</td>\n'.format(self.duration)
        html += u'		</tr>\n'
        html += u'		<tr>\n'
        html += u'			<td style="text-align: right;"><b>Total Tests</b></td>\n'
        html += u'			<td>{0}</td>\n'.format(len(self.test_results))
        html += u'		</tr>\n'
        html += u'		<tr>\n'
        html += u'			<td style="text-align: right;"><b>Passed Tests</b></td>\n'
        html += u'			<td>{0}</td>\n'.format(len(filter(lambda x: x.verdict, self.test_results)))
        html += u'		</tr>\n'
        html += u'		<tr>\n'
        html += u'			<td style="text-align: right;"><b>Failed Tests</b></td>\n'
        html += u'			<td>{0}</td>\n'.format(len(filter(lambda x: x.verdict is False, self.test_results)))
        html += u'		</tr>\n'
        html += u'		<tr>\n'
        html += u'			<td style="text-align: right;"><b>Tests with No Result</b></td>\n'
        html += u'			<td>{0}</td>\n'.format(len(filter(lambda x: x.verdict is None, self.test_results)))
        html += u'		</tr>\n'
        html += u'	</table>\n'
        html += u''
        html += u'	<h2>Detailed Results</h2>\n'
        html += u'	<table>\n'
        html += u'		<tr>\n'
        html += u'			<th>Test Name</th>\n'
        html += u'			<th>Verdict</th>\n'
        html += u'			<th>Duration</th>\n'
        html += u'		</tr>\n'
        for test_result in self.test_results:
            html += u'		<tr>\n'
            html += u'			<td>{0}</td>\n'.format(test_result.name)
            if test_result.verdict is None:
                html += u'			<td style="background-color: lightgray; color: gray;">No Result</td>\n'
            elif test_result.verdict is True:
                html += u'			<td style="background-color: lightgreen; color: green;">Pass</td>\n'
            else:
                html += u'			<td style="background-color: tomato; color: darkred;">Fail</td>\n'

            html += u'			<td>{0}s</td>\n'.format(test_result.duration)
            html += u'		</tr>\n'
        html += u'	</table>\n'
        html += u'  </body>\n'
        html += u'</html>\n'

        return html


class TestResult(object):

    def __init__(self, name):
        self.name = name
        self.__verdict = None
        self.start_time = None
        self.stop_time = None

    def start(self):
        self.start_time = datetime.now()

    def stop(self):
        self.stop_time = datetime.now()

    @property
    def duration(self):
        return (self.stop_time - self.start_time).total_seconds()

    @property
    def verdict(self):
        return self.__verdict

    @verdict.setter
    def verdict(self, value):
        self.__verdict = bool(value)
        if not self.stop_time:
            self.stop()
