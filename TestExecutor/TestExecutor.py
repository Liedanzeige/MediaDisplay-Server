import sys
import time
import os
import inspect
from datetime import datetime
import subprocess
import shutil

from TestReporter import TestResult, TestReporter

SCRIPT_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
TESTS_FILE = os.path.join(SCRIPT_DIR, 'Tests.txt')
SOURCE_DIR = os.path.normpath(os.path.join(SCRIPT_DIR, '..\\Source'))
TEST_EXECUTION_QUEUE = os.path.join(SOURCE_DIR, 'TestExecutionQueue.txt')
TEST_RESULT = os.path.join(SOURCE_DIR, 'TestResult.txt')
MAIN_PATH = os.path.join(SOURCE_DIR, 'main.py')


def get_test_cases():
    doubles = list()
    for line in open(TESTS_FILE, 'r').readlines():
        if line.strip() and not line.startswith('#') and line.strip() not in doubles:
            doubles.append(line.strip())
            yield line.strip()


def get_test_results_dir():
    now = datetime.now()
    return os.path.join(SCRIPT_DIR, 'Results', now.strftime("%Y-%m-%d_%H-%M-%S"))


def write_test_execution_queue(test):
    file_ = open(TEST_EXECUTION_QUEUE, 'w')
    file_.write(test)
    file_.close()


def delete_test_result_file():
    if os.path.exists(TEST_RESULT):
        os.remove(TEST_RESULT)


def save_test_result(test_result_dir):
    shutil.copy(TEST_RESULT, test_result_dir)


def save_data_to_file(data, path):
    open(path, 'w').write(data)


def execute_tests():
    start = datetime.now()
    successes = 0
    fails = 0
    test_results = list()

    test_results_dir = get_test_results_dir()
    os.makedirs(test_results_dir)
    os.chdir(SOURCE_DIR)

    for test in get_test_cases():
        delete_test_result_file()
        test_result_dir = os.path.join(test_results_dir, test)
        test_result = TestResult(test)
        os.makedirs(test_result_dir)
        write_test_execution_queue(test)
        test_result.start()
        process = subprocess.Popen([''"%s"'' % sys.executable, ''"%s"'' % MAIN_PATH],
                                   stderr=subprocess.PIPE,
                                   stdout=subprocess.PIPE)
        stdout_data, stderr_data = process.communicate()
        if not stderr_data and stdout_data and '] [  OK] 1 Tests finished 1 succeeded, 0 failed; time: ' in stdout_data.splitlines()[-1]:
            successes += 1
            test_result.verdict = True
        else:
            fails += 1
            test_result.verdict = False

        save_data_to_file(stdout_data, os.path.join(test_result_dir, 'stdout.txt'))
        save_data_to_file(stderr_data, os.path.join(test_result_dir, 'stderr.txt'))
        save_test_result(test_result_dir)
        test_results.append(test_result)
        time.sleep(10)
    save_data_to_file('successes: {0}, fails: {1}'.format(successes, fails),
                      os.path.join(test_results_dir, 'Result.txt'))

    duration = (datetime.now() - start).total_seconds()
    test_reporter = TestReporter(test_results, start, duration)
    html = test_reporter.get_html_report()
    html_result_path = os.path.join(test_results_dir, 'Result.html')
    save_data_to_file(html.encode('utf-8'), html_result_path)


if __name__ == '__main__':
    execute_tests()
