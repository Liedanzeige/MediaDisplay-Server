

import json
import os
import uuid

import pyqrcode

import Global
import Logger

DISPLAY_SERVER_OFF_TEXT = u'DisplayServer einschalten!'


class NetworkQtCodeHandler(object):

    def __init__(self, application):
        self.__application = application
        self.__dir_path = os.path.join(Global.CONST.USER_DATA_DIR, 'QrCodes')
        self.__image_dict_path = os.path.join(self.__dir_path, 'image_dict.json')
        if not os.path.isdir(self.__dir_path):
            os.makedirs(self.__dir_path)
        if not os.path.isfile(self.__image_dict_path):
            try:
                self.__write_image_dict({})
            except IOError as e:
                self.__application.logger.log(u'Error occurred writing image dict: %s' % unicode(e), __name__,
                                              Logger.SEVERITY_ERROR)
        self.__image_dict = dict()
        try:
            self.__image_dict = self.__read_image_dict()
        except IOError as e:
            self.__application.logger.log(u'Error occurred reading image dict: %s' % unicode(e), __name__,
                                          Logger.SEVERITY_ERROR)
            self.__image_dict = dict()

        self.create_image(DISPLAY_SERVER_OFF_TEXT)
        self.create_image(self.__get_url())

    def __read_image_dict(self):
        with open(self.__image_dict_path, 'r') as image_dict_file:
            image_dict = json.load(image_dict_file)
        new_image_dict = dict()
        for key, value in image_dict.iteritems():
            if os.path.isfile(os.path.join(self.__dir_path, value)):
                new_image_dict[key] = value
        return new_image_dict

    def __write_image_dict(self, image_dict):
        with open(self.__image_dict_path, 'w') as image_dict_file:
            json.dump(image_dict, image_dict_file)

    def get_url_text(self):
        if self.__application.general_settings.display_server == Global.CONST.STH.GENERAL.DISPLAY_SERVER_ON:
            return self.__get_url()
        return DISPLAY_SERVER_OFF_TEXT

    def __get_url(self):
        return u'http://' + self.__application.os_interface.get_ip() + u':' + unicode(Global.CONST.DS.PORT)

    def create_image(self, text):
        if text in self.__image_dict and \
                os.path.isfile(os.path.join(self.__dir_path, self.__image_dict[text])):
            return
        code = pyqrcode.create(text)
        file_name = self.__create_file_name()
        file_path = os.path.join(self.__dir_path, file_name)
        while file_name in os.listdir(self.__dir_path):
            file_name = self.__create_file_name()
        try:
            code.png(file_path, scale=40)
        except IOError as e:
            self.__application.logger.log(u'Error occurred generation QR-Code: %s' % unicode(e), __name__,
                                          Logger.SEVERITY_ERROR)
        else:
            self.__image_dict[text] = file_name
            try:
                self.__write_image_dict(self.__image_dict)
            except IOError as e:
                self.__application.logger.log(u'Error occurred writing image dict: %s' % unicode(e), __name__,
                                              Logger.SEVERITY_ERROR)

    @staticmethod
    def __create_file_name():
        return '{}.png'.format(str(uuid.uuid4()))

    def get_image_path(self, url_text):
        try:
            image_file_name = self.__image_dict[url_text]
        except KeyError:
            self.create_image(url_text)
            return os.path.join(self.__dir_path, self.__image_dict[url_text])
        else:
            image_path = os.path.join(self.__dir_path, image_file_name)
            if os.path.isfile(image_path):
                return image_path
            else:
                self.create_image(url_text)
                return os.path.join(self.__dir_path, self.__image_dict[url_text])
