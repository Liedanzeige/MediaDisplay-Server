# -*- coding: utf-8 -*-
"""Stellt den DisplayServer zur Verfügung."""

from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from SocketServer import ThreadingMixIn
import threading
import urllib2
import os
import shutil
import json
from datetime import datetime

import Global
import Logger
import DiagnosticModule


class DisplayRequestHandler(BaseHTTPRequestHandler):
    """Handelt Requests für den DisplayServer."""

    def log_error(self, format_, *args):
        """Log an arbitrary error message.

        This is used by all other logging functions.  Override
        it if you have specific logging wishes.

        The first argument, FORMAT, is a format string for the
        message to be logged.  If the format string contains
        any % escapes requiring parameters, they should be
        specified as subsequent arguments (it's just like
        printf!).

        The client ip address and current date/time are prefixed to every
        message.

        :param format_: Str format string for the message to be logged.
        :param args: Parameters for messages to be logged.
        """
        message = u"%s - - [%s] %s" % (self.client_address[0], self.log_date_time_string(), format_ % args)
        self.server.logger.log(message, __name__, Logger.SEVERITY_ERROR)

    def log_message(self, format_, *args):
        """Log an arbitrary message.

        This is used by all other logging functions.  Override
        it if you have specific logging wishes.

        The first argument, FORMAT, is a format string for the
        message to be logged.  If the format string contains
        any % escapes requiring parameters, they should be
        specified as subsequent arguments (it's just like
        printf!).

        The client ip address and current date/time are prefixed to every
        message.
        
        :param format_: Str format string for the message to be logged.
        :param args: Parameters for messages to be logged.
        """
        message = u"%s - - [%s] %s" % (self.client_address[0], self.log_date_time_string(), format_ % args)
        self.server.logger.log(message, __name__, Logger.SEVERITY_INFO)

    def version_string(self):
        """Return the server software version string."""
        return u'MediaDisplay-Server DisplayServer ' + Global.CONST.VERSION + u' ' + Global.CONST.BUILD

    @staticmethod
    def song_to_dict(song):
        """Wandelt ein Lied in ein Dictionary um.

        :param song: Lied, das in ein Dict umgewandelt werden soll
        :return: Dict, mit allen Attributen des Liedes
        """
        song_dict = dict()
        song_dict['number'] = song.number
        song_dict['book_abbreviation'] = song.book_abbreviation
        song_dict['book_color'] = song.book_color
        song_dict['book_id'] = song.book_id
        song_dict['title'] = song.title
        song_dict['sopran'] = song.sopran
        song_dict['alt'] = song.alt
        song_dict['tenor'] = song.tenor
        song_dict['bass'] = song.bass
        song_dict['sopran_name'] = song.sopran_name
        song_dict['alt_name'] = song.alt_name
        song_dict['tenor_name'] = song.tenor_name
        song_dict['bass_name'] = song.bass_name
        song_dict['general_song'] = song.general_song
        song_dict['date'] = song.date.isoformat()
        return song_dict

    def __generate_state(self):
        """Generiert einen String mit dem aktuellen Staus der Liedanzeige und einen mit dem aktuell angezeigtem Lied inkl. Solostimmen.

        :return: String, Integer, Object: Status, Statusnummer und zusätzliche Informationen wie z.B. das angezeigte Lied.
        """
        state_number = self.server.application.state_machine.state
        state = Global.CONST.SM.STATE_DICT.get(state_number, u'Unbekannt')
        if state_number == Global.CONST.SM.STATE_SONG:
            current_song = self.server.application.song_handler.get_latest_song()
            data = self.song_to_dict(current_song)
        elif state_number == Global.CONST.SM.STATE_NUMBER:
            data = self.server.application.screen_handler.get_cache()
        else:
            data = None
        return state, state_number, data

    def do_GET(self):
        """Serve a GET request."""

        decoded_url = urllib2.unquote(self.path).decode('utf-8')[1:]

        if decoded_url in Global.CONST.DS.STATIC_REQUESTS:
            self.__send_file(os.path.join(Global.CONST.DS.DOCUMENT_ROOT,
                                          Global.CONST.DS.STATIC_REQUESTS[decoded_url][0]),
                             Global.CONST.DS.STATIC_REQUESTS[decoded_url][1])
        elif decoded_url == 'erstetelefonnachricht':
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write("daspferdfrisstkeinengurkensalat")
        elif decoded_url == 'slave.json':
            if self.server.application.state_machine.state == Global.CONST.SM.STATE_SONG:
                current_song = self.server.application.song_handler.get_latest_song()
                if current_song.general_song:
                    if len(self.server.application.general_settings.general_song_books) >= 2:
                        json_answer = json.dumps({'state': 'song', 'data': {'book': current_song.book_abbreviation,
                                                                            'title': current_song.title,
                                                                            'color': current_song.book_color,
                                                                            'number': current_song.number}})
                    else:
                        json_answer = json.dumps({'state': 'song', 'data': {'title': current_song.title,
                                                                            'number': current_song.number}})
                else:
                    json_answer = json.dumps({'state': 'idle', 'data': None})
            elif self.server.application.state_machine.state == Global.CONST.SM.STATE_QUIT:
                if self.server.application.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN:
                    json_answer = json.dumps({'state': 'shutdown', 'data': None})
                else:
                    json_answer = json.dumps({'state': 'idle', 'data': None})
            elif self.server.application.state_machine.state == Global.CONST.SM.STATE_STANDBY:
                json_answer = json.dumps({'state': 'standby', 'data': None})
            else:
                json_answer = json.dumps({'state': 'idle', 'data': None})
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(json_answer)
            self.server.application.display_server.last_slave_request = datetime.now()
        elif decoded_url == 'weather.json':
            temp = self.server.application.os_interface.temperature
            humidity = self.server.application.os_interface.humidity
            pressure = self.server.application.os_interface.pressure
            date = unicode(datetime.now())
            json_answer = json.dumps({"temp": temp, "pressure": pressure, "humidity": humidity, "date": date})
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(json_answer)
        elif decoded_url == 'state.json':
            state, state_number, data = self.__generate_state()
            json_answer = json.dumps({"state": state, "state_number": state_number, "data": data})
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(json_answer)
        elif decoded_url == 'sungsongs.json':
            sung_songs = sorted(self.server.application.song_handler.get_sung_songs(),
                                key=lambda a: a.date, reverse=True)
            song_dicts = list()
            for song in sung_songs:
                song_dicts.append(self.song_to_dict(song))
            json_answer = json.dumps(song_dicts)
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(json_answer)
        elif decoded_url == 'datetime.json':
            json_answer = {'datetime': datetime.now().isoformat()}
            json_answer = json.dumps(json_answer)
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(json_answer)
        elif decoded_url == 'version.json':
            json_answer = Global.CONST.DS.API_VERSION
            json_answer = json.dumps(json_answer)
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(json_answer)
        elif decoded_url == 'applicationversion.json':
            json_answer = Global.CONST.VERSION
            json_answer = json.dumps(json_answer)
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(json_answer)
        elif decoded_url == 'applicationbuildnumber.json':
            json_answer = Global.CONST.BUILD
            json_answer = json.dumps(json_answer)
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(json_answer)
        else:
            print decoded_url
            self.log_error(u'Unknown request: %s', decoded_url)
            self.send_error(404, 'Nothing matches the given URI')
            self.wfile.write('Wolltest du mich testen oder was?')

    def __send_file(self, path, mime_type):
        """Beantwortet einen Request mit einer Datei.

        :param path: String, Pfad zur angeforderten Datei.
        :param mime_type: String, Mime-Typ der angeforderten Datei.
        """
        read_file = None
        try:
            read_file = open(path, 'rb')
        except IOError:
            self.send_error(404, "File not found")
        else:
            self.send_response(200)
            self.send_header("Content-type", mime_type)
            fs = os.fstat(read_file.fileno())
            self.send_header("Content-Length", str(fs[6]))
            self.send_header("Last-Modified", self.date_time_string(fs.st_mtime))
            self.end_headers()
            shutil.copyfileobj(read_file, self.wfile)
        finally:
            if read_file:
                read_file.close()


class DisplayServer(ThreadingMixIn, HTTPServer):
    """Implementiert einen Server zur Anzeige des Zustands der Liedanzeige für den Tontechniker."""

    def __init__(self, application, server_address, request_handler_class, bind_and_activate, log_dir):
        HTTPServer.__init__(self, server_address, request_handler_class, bind_and_activate)
        self.application = application
        self.__thread = None
        self.last_slave_request = datetime.now()
        self.logger = Logger.Logger(log_dir, 'DisplayServer', 1)
        self.logger.cleanup()

    def is_running(self):
        """Gibt zurück ob der Server läuft.
        
        :return: Boolean, True wenn der Server läuft, sonst False.
        """
        if self.__thread is None:
            return False
        else:
            return self.__thread.is_alive()

    def start(self):
        """Startet den Server, wenn er noch nicht läuft."""
        if not self.is_running():
            self.__thread = threading.Thread(target=self.serve, args=(), name='DisplayServer')
            self.__thread.start()
        self.logger.start()
        self.logger.log('DisplayServer started', __name__, Logger.SEVERITY_INFO)

    @DiagnosticModule.execute_with_exception_handling
    def serve(self):
        self.serve_forever()

    def stop(self):
        """Stoppt den Server."""
        if self.is_running():
            self.shutdown()
        self.logger.stop()
        self.logger.log('DisplayServer stopped', __name__, Logger.SEVERITY_INFO)


if __name__ == '__main__':
    # This is just to test the DisplayServer
    import random
    import SongHandler
    import SettingsHandler
    import ScreenHandler

    class MockOsInterface(object):
        def __init__(self):
            pass

        @property
        def temperature(self):
            return random.gauss(22.0, 1)

        @property
        def humidity(self):
            return random.gauss(34.0, 5)

        @property
        def pressure(self):
            return random.gauss(1010.0, 10)

        @staticmethod
        def get_ip():
            return '127.0.0.1'

    class MockUpApplication(object):
        def __init__(self):
            self.general_settings = None
            self.os_interface = MockOsInterface()
            self.song_handler = SongHandler.SongHandler(self)
            self.state_machine = MockUpStateMachine()


    class MockUpStateMachine(object):
        def __init__(self):
            self.state = 2

    app = MockUpApplication()
    screen_handler = ScreenHandler.ScreenHandler(app)

    try:
        screen_handler.add_number('0')
    except AttributeError:
        pass
    try:
        screen_handler.add_number('4')
    except AttributeError:
        pass
    try:
        screen_handler.add_number('+')
    except AttributeError:
        pass
    try:
        screen_handler.add_number('1')
    except AttributeError:
        pass
    try:
        screen_handler.add_number('2')
    except AttributeError:
        pass
    try:
        screen_handler.add_number('3')
    except AttributeError:
        pass
    general_settings = SettingsHandler.GeneralSettingsHandler(app)
    general_settings.general_song_books = '04'
    song_factory = SongHandler.SongFactory(None)
    app.song_handler.sing_song(song_factory.createSong(u'1 12'))
    app.song_handler.sing_song(song_factory.createSong(u'2 56 3'))
    app.song_handler.sing_song(song_factory.createSong(u'123'))
    app.song_handler.sing_song(song_factory.createSong(u'04 123 1234'))
    display_server = DisplayServer(app, (u"", Global.CONST.DS.PORT), DisplayRequestHandler, True, '.')
    display_server.start()
    print u'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
    print u'<                                        >'
    print u'<     DisplayServer Test Application     >'
    print u'<                                        >'
    print u'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
    print u'Enter number to change state'
    print u'Enter \'s\' followed by song-string to set a song'
    print u'Enter \'a\' followed by books-string to set general song books'
    print u'Enter \'exit\' to exit'
    while True:
        try:
            answer = raw_input(u'>>> ')
            if answer.isdigit():
                app.state_machine.state = int(answer)
            elif answer.startswith(u's'):
                app.song_handler.sing_song(song_factory.createSong(answer[1:]))
                app.state_machine.state = 2
            elif answer.startswith(u'a'):
                general_settings.general_song_books = answer[1:]
            elif answer == u'exit':
                break
        except Exception as e:
            print e
            break
    display_server.stop()
