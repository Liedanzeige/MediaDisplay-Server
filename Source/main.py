# -*- coding: utf-8 -*-

import os
import sys
import time
from PySide import QtGui
from PySide import QtCore

import Global
import DisplayServer
import ScreenHandler
import StateMachine
import OsInterface
import TestRunner
import Logger
import Watchdog
import MDSWidgets
import SettingsHandler
import SongHandler
import UpdateHandler
import DiagnosticModule


class MainWindow(QtGui.QWidget):
    """GUI der Anwendung."""

    # Mit diesem Signal wird die Seite der GUI gesetzt. Aufruf über
    # MainWindow.set_stack_index.emit(int)
    set_stack_index = QtCore.Signal(int)

    def __init__(self, application):
        super(MainWindow, self).__init__()
        self.__application = application
        self.ids = dict()
        self.__current = 0
        self.stack = QtGui.QStackedLayout()
        self.set_stack_index[int].connect(self.__set_stack_index)
        self.init_gui()

    @property
    def current(self):
        """Der Index der aktuellen Seite. Typ int."""
        return self.__current

    @current.setter
    def current(self, value):
        self.set_stack_index.emit(value)

    def __set_stack_index(self, index):
        self.stack.setCurrentIndex(index)
        self.__current = index

    def init_gui(self):
        """Initialisiert die Widgets der GUI."""

        # Start / 0
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel('')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        label2 = MDSWidgets.StretchedLabel('Liedanzeige')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        self.ids['LabelStartLiedanzeige'] = label2
        label3 = MDSWidgets.StretchedLabel(u'   MediaDisplay-Server +++ release full name +++   ')
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        self.ids['LabelStartVersion'] = label3
        label4 = MDSWidgets.StretchedLabel(u'   \u00A9 +++ replace year +++ Liedanzeige Entwicklungsteam   ')
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        self.ids['LabelStartCopyright'] = label4
        label4_1 = MDSWidgets.StretchedLabel(u'Liedanzeige startet...')
        label4_1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                               Global.CONST.SCH.COLOR.FG + u"; }")
        self.ids['LabelStartState'] = label4_1
        label5 = MDSWidgets.StretchedLabel('')
        label5.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label1, 1, 0, 1, 1)
        grid.addWidget(label2, 2, 0, 2, 1)
        grid.addWidget(label3, 4, 0, 1, 1)
        grid.addWidget(label4, 5, 0, 1, 1)
        grid.addWidget(label4_1, 6, 0, 1, 1)
        grid.addWidget(label5, 7, 0, 1, 1)
        widget.setLayout(grid)

        self.stack.addWidget(widget)

        # Zeige Uhr / 1
        if self.__application.general_settings.clock_idle == Global.CONST.STH.GENERAL.CLOCK_IDLE_ANALOG:
            clock = MDSWidgets.AnalogClock()
            self.stack.addWidget(clock)
        elif self.__application.general_settings.clock_idle == Global.CONST.STH.GENERAL.CLOCK_IDLE_DIGITAL:
            clock = MDSWidgets.DigitalClock('%H:%M')
            clock.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                Global.CONST.SCH.COLOR.FG + u"; }")
            self.stack.addWidget(clock)
        elif self.__application.general_settings.clock_idle == Global.CONST.STH.GENERAL.CLOCK_IDLE_LIVE_STUDIO:
            widget = QtGui.QWidget()
            grid = QtGui.QGridLayout()
            grid.setSpacing(0)
            grid.setContentsMargins(0, 0, 0, 0)

            empty_label = MDSWidgets.StretchedLabel(u'')
            empty_label.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                      Global.CONST.SCH.COLOR.CLOCK_STUDIO_FG + u"; }")
            grid.addWidget(empty_label, 0, 0, 1, 3)

            clock = MDSWidgets.DigitalClock('%H:%M:%S')
            clock.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                Global.CONST.SCH.COLOR.CLOCK_STUDIO_FG + u"; }")
            clock.setFont(QtGui.QFont("OPTICalculator", 20))
            grid.addWidget(clock, 1, 0, 2, 3)

            label_temp = MDSWidgets.TemperatureLabel(self.__application.os_interface)
            label_temp.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                     Global.CONST.SCH.COLOR.CLOCK_STUDIO_FG + u"; }")
            label_temp.setFont(QtGui.QFont("OPTICalculator", 20))
            grid.addWidget(label_temp, 3, 0, 1, 1)

            label_humid = MDSWidgets.HumidityLabel(self.__application.os_interface)
            label_humid.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                      Global.CONST.SCH.COLOR.CLOCK_STUDIO_FG + u"; }")
            label_humid.setFont(QtGui.QFont("OPTICalculator", 20))
            grid.addWidget(label_humid, 3, 1, 1, 1)

            label_pressure = MDSWidgets.PressureLabel(self.__application.os_interface)
            label_pressure.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                         Global.CONST.SCH.COLOR.CLOCK_STUDIO_FG + u"; }")
            label_pressure.setFont(QtGui.QFont("OPTICalculator", 20))
            grid.addWidget(label_pressure, 3, 2, 1, 1)

            widget.setLayout(grid)
            self.stack.addWidget(widget)
        else:
            widget = QtGui.QWidget()
            grid = QtGui.QGridLayout()
            widget.setLayout(grid)
            grid.setSpacing(0)
            grid.setContentsMargins(0, 0, 0, 0)
            empty_widget = MDSWidgets.StretchedLabel(u'')
            empty_widget.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                       Global.CONST.SCH.COLOR.FG + u"; }")
            grid.addWidget(empty_widget, 0, 0, 11, 1)
            verse_widget = MDSWidgets.StretchedLabel(u'Meine Zeit steht in deinen Händen.')
            verse_widget.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                       Global.CONST.SCH.COLOR.FG + u"; }")
            grid.addWidget(verse_widget, 11, 0, 1, 1)
            self.stack.addWidget(widget)

        # Zeige Lied / 2
        label1_2 = MDSWidgets.StretchedLabel('---')
        label1_2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                               Global.CONST.SCH.COLOR.FG + u"; }")
        self.ids['SongNumber'] = label1_2
        grid1_1 = QtGui.QGridLayout()
        grid1_1.setSpacing(0)
        grid1_1.setContentsMargins(0, 0, 0, 0)
        # grid1_1.addWidget(label1_1, 0, 0, 1, 1)
        grid1_1.addWidget(label1_2, 0, 0, 1, 4)
        widget1_1 = QtGui.QWidget()
        widget1_1.setLayout(grid1_1)

        label_sopran = MDSWidgets.StretchedLabel('Sopran')
        label_sopran.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SOLO_SOPRAN_BG +
                                   u"; color : " + Global.CONST.SCH.COLOR.SOLO_SOPRAN_FG + u"; }")
        self.ids['Sopran'] = label_sopran
        label_alt = MDSWidgets.StretchedLabel('Alt')
        label_alt.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SOLO_ALT_BG +
                                u"; color : " + Global.CONST.SCH.COLOR.SOLO_ALT_FG + u"; }")
        self.ids['Alt'] = label_alt
        label_tenor = MDSWidgets.StretchedLabel('Tenor')
        label_tenor.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SOLO_TENOR_BG +
                                  u"; color : " + Global.CONST.SCH.COLOR.SOLO_TENOR_FG + u"; }")
        self.ids['Tenor'] = label_tenor
        label_bass = MDSWidgets.StretchedLabel('Bass')
        label_bass.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SOLO_BASS_BG +
                                 u"; color : " + Global.CONST.SCH.COLOR.SOLO_BASS_FG + u"; }")
        self.ids['Bass'] = label_bass
        label_title = MDSWidgets.MarqueeLabel(' --- ')
        label_title.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG +
                                  u"; color : " + Global.CONST.SCH.COLOR.FG + u"; }")
        self.ids['Title'] = label_title
        grid2_1 = QtGui.QGridLayout()
        grid2_1.setSpacing(0)
        grid2_1.setContentsMargins(0, 0, 0, 0)
        widget2_1 = QtGui.QWidget()
        widget2_1.setLayout(grid2_1)

        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)

        if self.__application.general_settings.clock_song == Global.CONST.STH.GENERAL.CLOCK_SONG_NO:
            if self.__application.general_settings.order_solo == Global.CONST.STH.GENERAL.ORDER_SOLO_NORMAL:
                grid2_1.addWidget(label_sopran, 0, 1, 1, 1)
                grid2_1.addWidget(label_alt, 0, 2, 1, 1)
                grid2_1.addWidget(label_tenor, 0, 0, 1, 1)
                grid2_1.addWidget(label_bass, 0, 3, 1, 1)
                grid.addWidget(widget1_1, 0, 0, 4, 1)
                grid.addWidget(widget2_1, 4, 0, 1, 1)

            elif self.__application.general_settings.order_solo == Global.CONST.STH.GENERAL.ORDER_SOLO_MIRRORED:
                grid2_1.addWidget(label_sopran, 0, 2, 1, 1)
                grid2_1.addWidget(label_alt, 0, 1, 1, 1)
                grid2_1.addWidget(label_tenor, 0, 3, 1, 1)
                grid2_1.addWidget(label_bass, 0, 0, 1, 1)
                grid.addWidget(widget1_1, 0, 0, 4, 1)
                grid.addWidget(widget2_1, 4, 0, 1, 1)

            elif self.__application.general_settings.order_solo == Global.CONST.STH.GENERAL.ORDER_SOLO_TITLE:
                grid2_1.addWidget(label_title, 0, 0, 1, 1)
                grid.addWidget(widget1_1, 0, 0, 3, 1)
                grid.addWidget(widget2_1, 4, 0, 2, 1)

            else:
                grid2_1.addWidget(label_sopran, 0, 0, 1, 1)
                grid2_1.addWidget(label_alt, 0, 1, 0, 1)
                grid2_1.addWidget(label_tenor, 0, 2, 1, 1)
                grid2_1.addWidget(label_bass, 0, 3, 1, 1)
                grid.addWidget(widget1_1, 0, 0, 4, 1)
                grid.addWidget(widget2_1, 4, 0, 1, 1)

        else:
            if self.__application.general_settings.order_solo == Global.CONST.STH.GENERAL.ORDER_SOLO_NORMAL:
                grid2_1.addWidget(label_sopran, 1, 1, 1, 1)
                grid2_1.addWidget(label_alt, 1, 0, 1, 1)
                grid2_1.addWidget(label_tenor, 0, 1, 1, 1)
                grid2_1.addWidget(label_bass, 0, 0, 1, 1)

            elif self.__application.general_settings.order_solo == Global.CONST.STH.GENERAL.ORDER_SOLO_MIRRORED:
                grid2_1.addWidget(label_sopran, 1, 0, 1, 1)
                grid2_1.addWidget(label_alt, 1, 1, 1, 1)
                grid2_1.addWidget(label_tenor, 0, 0, 1, 1)
                grid2_1.addWidget(label_bass, 0, 1, 1, 1)

            elif self.__application.general_settings.order_solo == Global.CONST.STH.GENERAL.ORDER_SOLO_TITLE:
                grid2_1.addWidget(label_title, 0, 0, 1, 1)

            else:
                grid2_1.addWidget(label_sopran, 0, 0, 1, 1)
                grid2_1.addWidget(label_alt, 0, 1, 1, 1)
                grid2_1.addWidget(label_tenor, 1, 0, 1, 1)
                grid2_1.addWidget(label_bass, 1, 1, 1, 1)

            label3_1 = MDSWidgets.DigitalClock('%H:%M')
            label3_1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                   Global.CONST.SCH.COLOR.FG + u"; }")
            self.ids['SongScreenClock'] = label3_1
            grid3_1 = QtGui.QGridLayout()
            grid3_1.setSpacing(0)
            grid3_1.setContentsMargins(0, 0, 0, 0)
            grid3_1.addWidget(label3_1, 0, 0, 0, 0)
            widget3_1 = QtGui.QWidget()
            widget3_1.setLayout(grid3_1)

            grid.addWidget(widget1_1, 0, 0, 2, 2)
            grid.addWidget(widget2_1, 2, 0, 1, 1)
            grid.addWidget(widget3_1, 2, 1, 1, 1)

        widget = QtGui.QWidget()
        widget.setLayout(grid)

        self.stack.addWidget(widget)

        # Zeige Nummer / 3
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel('---')
        label1.setFont(QtGui.QFont("OPTICalculator", 20))
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        self.ids['Number'] = label1
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Bluescreen / 4
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel(u' Oops... ')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BLUESCREEN + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label1, 0, 0, 3, 1)
        self.ids['Oops'] = label1
        label2 = MDSWidgets.StretchedLabel(u' Das h\u00e4tte nicht passieren d\u00fcrfen ')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BLUESCREEN + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label2, 3, 0, 1, 1)
        self.ids['BluescreenLabel'] = label2
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Standby / 5
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel('Standby')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        self.ids['StandbyLabel'] = label1
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Beenden / 6
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel('Beenden...')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        self.ids['LabelQuit'] = label1
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Bestätige Beenden / 7
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsConfirmQuitList'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Beenden?')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label1, 0, 0, 2, 1)
        self.ids['LabelsConfirmQuitList'].append(label1)
        label2 = MDSWidgets.StretchedLabel(u'                  1 - Standby                  ')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label2, 2, 0, 1, 1)
        self.ids['LabelsConfirmQuitList'].append(label2)
        label3 = MDSWidgets.StretchedLabel(u'          2 oder OK - Herunterfahren           ')
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label3, 3, 0, 1, 1)
        self.ids['LabelsConfirmQuitList'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'                  3 - Neustart                 ')
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label4, 4, 0, 1, 1)
        self.ids['LabelsConfirmQuitList'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle Uhr / 8
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetTimeList'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Stelle Uhr')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        self.ids['LabelsSetTimeList'].append(label1)
        label3 = MDSWidgets.DigitalClock('%H:%M:%S')
        label3.setFont(QtGui.QFont("DejaVu Sans Mono", 20))
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_OK + u"; }")
        grid.addWidget(label3, 2, 0, 1, 1)
        self.ids['LabelsSetTimeList'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'--:--:--')
        label4.setFont(QtGui.QFont("DejaVu Sans Mono", 20))
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_EDIT + u"; }")
        self.ids['LabelSetTime'] = label4
        grid.addWidget(label4, 3, 0, 1, 1)
        self.ids['LabelsSetTimeList'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle Datum / 9
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetDateList'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Stelle Datum')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        self.ids['LabelsSetDateList'].append(label1)
        label3 = MDSWidgets.DigitalClock('%d.%m.%Y')
        label3.setFont(QtGui.QFont("DejaVu Sans Mono", 20))
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_OK + u"; }")
        grid.addWidget(label3, 2, 0, 1, 1)
        self.ids['LabelsSetDateList'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'--.--.----')
        label4.setFont(QtGui.QFont("DejaVu Sans Mono", 20))
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_EDIT + u"; }")
        self.ids['LabelSetDate'] = label4
        grid.addWidget(label4, 3, 0, 1, 1)
        self.ids['LabelsSetDateList'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle Uhranzeige Idle / 10
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetClockIdleList'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Uhranzeige')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 2, 3)
        self.ids['LabelsSetClockIdleList'].append(label1)
        label2 = MDSWidgets.StretchedLabel(u'1 - analog ')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label2, 2, 1, 1, 2)
        self.ids['LabelsSetClockIdleList'].append(label2)
        label3 = MDSWidgets.StretchedLabel(u'2 - digital ')
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label3, 3, 1, 1, 2)
        label4 = MDSWidgets.StretchedLabel(u'3 - ohne   ')
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label4, 4, 1, 1, 2)
        self.ids['LabelsSetClockIdleList'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'  ---  ')
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        self.ids['LabelSetClockIdle'] = label4
        grid.addWidget(label4, 2, 0, 3, 1)
        self.ids['LabelsSetClockIdleList'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle Uhranzeige Lied / 11
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetClockSongList'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Liedanzeige')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 3)
        self.ids['LabelsSetClockSongList'].append(label1)
        label2 = MDSWidgets.StretchedLabel(u' 1 - ohne Uhr ')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label2, 1, 1, 1, 2)
        self.ids['LabelsSetClockSongList'].append(label2)
        label3 = MDSWidgets.StretchedLabel(u' 2 - mit Uhr  ')
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label3, 2, 1, 1, 2)
        self.ids['LabelsSetClockSongList'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'  ---  ')
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        self.ids['LabelSetClockSong'] = label4
        grid.addWidget(label4, 1, 0, 2, 1)
        self.ids['LabelsSetClockSongList'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle Übergangszeit Nummer -> Lied / 12
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetTime_3_2_List'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Eingabedauer Lied')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 2)
        self.ids['LabelsSetTime_3_2_List'].append(label1)
        label3 = MDSWidgets.StretchedLabel(u'Sekunden')
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label3, 1, 1, 2, 1)
        self.ids['LabelsSetTime_3_2_List'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'  ---  ')
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        self.ids['LabelSetTime_3_2'] = label4
        grid.addWidget(label4, 1, 0, 2, 1)
        self.ids['LabelsSetTime_3_2_List'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle Übergangszeit Lied -> Uhr / 13
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetTime_2_1_List'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Anzeigedauer Lied')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 2)
        self.ids['LabelsSetTime_2_1_List'].append(label1)
        label3 = MDSWidgets.StretchedLabel(u'Sekunden')
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label3, 1, 1, 2, 1)
        self.ids['LabelsSetTime_2_1_List'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'  ---  ')
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        self.ids['LabelSetTime_2_1'] = label4
        grid.addWidget(label4, 1, 0, 2, 1)
        self.ids['LabelsSetTime_2_1_List'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle Übergangszeit Uhr -> Standby / 14
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetTime_1_5_List'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Wartezeit Standby')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 2)
        self.ids['LabelsSetTime_1_5_List'].append(label1)
        label3 = MDSWidgets.StretchedLabel(u'Minuten')
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label3, 1, 1, 2, 1)
        self.ids['LabelsSetTime_1_5_List'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'  ---  ')
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        self.ids['LabelSetTime_1_5'] = label4
        grid.addWidget(label4, 1, 0, 2, 1)
        self.ids['LabelsSetTime_1_5_List'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle LogLevel / 15
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel(u'Stelle LogLevel')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Seite Systeminformationen / 16
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSysteminformation'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Systeminformationen')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 2, 1)
        self.ids['LabelsSysteminformation'].append(label1)
        label2 = MDSWidgets.StretchedLabel(u'Version: ' + Global.CONST.VERSION)
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label2, 2, 0, 1, 1)
        self.ids['LabelsSysteminformation'].append(label2)
        label3 = MDSWidgets.StretchedLabel(u'Build: ' + Global.CONST.BUILD)
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label3, 3, 0, 1, 1)
        self.ids['LabelsSysteminformation'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'Commit: ' + Global.CONST.COMMIT)
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label4, 4, 0, 1, 1)
        self.ids['LabelsSysteminformation'].append(label4)
        label5 = MDSWidgets.StretchedLabel(u'Seriennummer des Prozessors: ' +
                                           self.__application.os_interface.serial_number)
        label5.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label5, 5, 0, 1, 1)
        self.ids['LabelsSysteminformation'].append(label5)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Zeige IP / 17
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)

        label0 = MDSWidgets.StretchedLabel(u'Zum Verbinden scannen:')
        label0.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label0, 0, 0, 1, 1)

        label1 = MDSWidgets.StretchedImage()
        label1.set_image.emit(Global.CONST.SCH.ERROR_QR_CODE_PATH)
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.BG + u"; }")
        self.ids['LabelNetworkQrCode'] = label1
        grid.addWidget(label1, 1, 0, 8, 1)

        label2 = MDSWidgets.StretchedLabel(u'Netzwerkadresse')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        self.ids['LabelNetworkAddress'] = label2
        grid.addWidget(label2, 9, 0, 1, 1)

        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle DisplayServer
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetDisplayServerList'] = list()
        label1 = MDSWidgets.StretchedLabel(u'DisplayServer')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 3)
        self.ids['LabelsSetDisplayServerList'].append(label1)
        label2 = MDSWidgets.StretchedLabel(u' 1 - Ein ')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label2, 1, 1, 1, 2)
        self.ids['LabelsSetDisplayServerList'].append(label2)
        label3 = MDSWidgets.StretchedLabel(u' 2 - Aus ')
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label3, 2, 1, 1, 2)
        self.ids['LabelsSetDisplayServerList'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'  ---  ')
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        self.ids['LabelSetDisplayServer'] = label4
        grid.addWidget(label4, 1, 0, 2, 1)
        self.ids['LabelsSetDisplayServerList'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle Allg. Liederbücher
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetGeneralSongBooks'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Allg. Liederbücher')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        self.ids['LabelsSetGeneralSongBooks'].append(label1)
        label2 = MDSWidgets.StretchedLabel(u'---')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        self.ids['LabelSetGeneralSongBooks'] = label2
        grid.addWidget(label2, 1, 0, 2, 1)
        self.ids['LabelsSetGeneralSongBooks'].append(label2)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle Reihenfolge Solisten
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetOrderSolo'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Reihenfolge Solisten')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        self.ids['LabelsSetOrderSolo'].append(label1)
        label2 = MDSWidgets.StretchedLabel(u'---')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        self.ids['LabelSetOrderSolo'] = label2
        grid.addWidget(label2, 1, 0, 2, 1)
        self.ids['LabelsSetOrderSolo'].append(label2)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # 21 Bestätige Update
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsConfirmUpdate'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Wenn sie das Update ausf\u00fchren wollen geben sie '
                                           u'folgende Zahlenfolge ein:')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        self.ids['LabelsConfirmUpdate'].append(label1)
        label2 = MDSWidgets.StretchedLabel(UpdateHandler.get_update_confirmation_number())
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label2, 1, 0, 1, 1)
        self.ids['LabelsConfirmUpdate'].append(label2)
        label3 = MDSWidgets.StretchedLabel(u'---')
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_EDIT + u"; }")
        self.ids['LabelConfirmUpdate'] = label3
        grid.addWidget(label3, 2, 0, 1, 1)
        self.ids['LabelsConfirmUpdate'].append(label3)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Zeige Nachricht / 22
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)

        empty_widget = MDSWidgets.StretchedLabel(u'')
        empty_widget.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                   Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(empty_widget, 0, 0, 1, 1)

        message_label = MDSWidgets.MarqueeLabel(u'---')
        message_label.setTextFormat(QtCore.Qt.TextFormat.PlainText)
        message_label.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG +
                                    u"; color : " + Global.CONST.SCH.COLOR.FG + u"; }")
        self.ids['Message'] = message_label
        grid.addWidget(message_label, 1, 0, 5, 1)

        empty_widget = MDSWidgets.StretchedLabel(u'')
        empty_widget.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                   Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(empty_widget, 6, 0, 1, 1)

        widget.setLayout(grid)
        self.stack.addWidget(widget)

        self.current = Global.CONST.SM.STATE_START

        self.setLayout(self.stack)

        self.stack.setCurrentIndex(0)
        # self.setGeometry(300, 300, 700, 500)
        self.setWindowTitle(u'Liedanzeige')

        self.showFullScreen()

        # Zeige On Air / 23
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)

        label_on_air = MDSWidgets.TextChangingLabel(u'ON AIR', u'\u2022 ON AIR \u2022')
        label_on_air.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.ON_AIR_FG + u"; color : " +
                                   Global.CONST.SCH.COLOR.CLOCK_STUDIO_FG + u"; }")
        grid.addWidget(label_on_air, 0, 0, 1, 3)

        clock = MDSWidgets.DigitalClock('%H:%M:%S')
        clock.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                            Global.CONST.SCH.COLOR.CLOCK_STUDIO_FG + u"; }")
        clock.setFont(QtGui.QFont("OPTICalculator", 20))
        grid.addWidget(clock, 1, 0, 2, 3)

        label_temp = MDSWidgets.TemperatureLabel(self.__application.os_interface)
        label_temp.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                 Global.CONST.SCH.COLOR.CLOCK_STUDIO_FG + u"; }")
        label_temp.setFont(QtGui.QFont("OPTICalculator", 20))
        grid.addWidget(label_temp, 3, 0, 1, 1)

        label_humid = MDSWidgets.HumidityLabel(self.__application.os_interface)
        label_humid.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                  Global.CONST.SCH.COLOR.CLOCK_STUDIO_FG + u"; }")
        label_humid.setFont(QtGui.QFont("OPTICalculator", 20))
        grid.addWidget(label_humid, 3, 1, 1, 1)

        label_pressure = MDSWidgets.PressureLabel(self.__application.os_interface)
        label_pressure.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                     Global.CONST.SCH.COLOR.CLOCK_STUDIO_FG + u"; }")
        label_pressure.setFont(QtGui.QFont("OPTICalculator", 20))
        grid.addWidget(label_pressure, 3, 2, 1, 1)

        widget.setLayout(grid)
        self.stack.addWidget(widget)


class MediaDisplayServerApp(QtGui.QApplication):
    """MediaDisplay-Server Anwendung."""

    def __init__(self, *args, **kwargs):
        super(MediaDisplayServerApp, self).__init__(*args, **kwargs)
        self.__log_dir = os.path.join(Global.CONST.USER_DATA_DIR, 'log')
        self.watchdog = None
        self.os_interface = None
        self.screen_handler = None
        self.state_machine = None
        self.song_handler = None
        self.display_server = None
        self.test_runner = None
        self.logger = Logger.Logger(self.__log_dir, 'MediaDisplay-Server')
        DiagnosticModule.DiagnosticModule(self.__log_dir)
        self.general_settings = SettingsHandler.GeneralSettingsHandler(self)
        self.font_database = QtGui.QFontDatabase()
        self.load_fonts()
        self.init_instances()
        self.main_window = MainWindow(self)
        self.logger.log('Start state machine', __name__, Logger.SEVERITY_DEBUG)
        self.state_machine.start()
        self.logger.log('State machine started', __name__, Logger.SEVERITY_DEBUG)
        # noinspection PyUnresolvedReferences
        self.aboutToQuit.connect(self.on_stop)

    def load_fonts(self):
        """Lädt die Schriftarten für die GUI."""
        for dirname, dirs, files in os.walk(Global.CONST.FONTS_DIR):
            for file in files:
                if file.lower().endswith('.ttf') or file.lower().endswith('.otf'):
                    font_path = os.path.join(dirname, file)
                    self.font_database.addApplicationFont(font_path)

    def on_stop(self):
        """Stoppt alle Instanzen."""
        self.logger.log('Stop instances...', __name__, Logger.SEVERITY_DEBUG)

        self.logger.log('Stop Watchdog...', __name__, Logger.SEVERITY_DEBUG)
        self.watchdog.stop()
        self.logger.log('Watchdog stopped', __name__, Logger.SEVERITY_DEBUG)

        self.logger.log('Stop StateMachine...', __name__, Logger.SEVERITY_DEBUG)
        self.state_machine.stop()
        self.logger.log('StateMachine stopped', __name__, Logger.SEVERITY_DEBUG)

        self.logger.log('Stop DisplayServer...', __name__, Logger.SEVERITY_DEBUG)
        self.display_server.stop()
        self.logger.log('DisplayServer stopped', __name__, Logger.SEVERITY_DEBUG)

        self.logger.log('Stop OsInterface...', __name__, Logger.SEVERITY_DEBUG)
        self.os_interface.stop()
        self.logger.log('OsInterface stopped', __name__, Logger.SEVERITY_DEBUG)

        self.logger.log('Stop Logger...', __name__, Logger.SEVERITY_DEBUG)
        self.logger.stop()
        self.logger.log('Logger stopped', __name__, Logger.SEVERITY_DEBUG)

        self.logger.log('All instances stopped', __name__, Logger.SEVERITY_DEBUG)
        self.logger.log('Exit Program', __name__, Logger.SEVERITY_DEBUG)

    def init_instances(self):
        """Initialisiert alle Instanzen."""
        self.init_dirs()
        self.logger.log('MediaDisplay-Server %s %s' % (Global.CONST.VERSION, Global.CONST.BUILD),
                        __name__, Logger.SEVERITY_INFO)
        self.logger.log('Start init instances...', __name__, Logger.SEVERITY_DEBUG)
        self.watchdog = Watchdog.Watchdog(self)
        self.os_interface = OsInterface.OsInterface(self)
        self.screen_handler = ScreenHandler.ScreenHandler(self)
        self.state_machine = StateMachine.StateMachine(self)
        self.song_handler = SongHandler.SongHandler(self)
        self.display_server = DisplayServer.DisplayServer(self, ("", Global.CONST.DS.PORT),
                                                          DisplayServer.DisplayRequestHandler,
                                                          True,
                                                          self.__log_dir)
        if Global.CONST.TEST:
            self.test_runner = TestRunner.TestRunner(self)

        self.logger.cleanup()
        self.logger.start()
        self.logger.log('Instances inited', __name__, Logger.SEVERITY_DEBUG)

    def start_instances(self):
        """Startet alle Instanzen."""
        self.logger.log('Start DisplayServer...', __name__, Logger.SEVERITY_DEBUG)
        if self.general_settings.display_server == Global.CONST.STH.GENERAL.DISPLAY_SERVER_ON:
            self.display_server.start()
        self.logger.log('DisplayServer started', __name__, Logger.SEVERITY_DEBUG)

        self.logger.log('Start OsInterface...', __name__, Logger.SEVERITY_DEBUG)
        self.os_interface.start()
        self.logger.log('OsInterface started', __name__, Logger.SEVERITY_DEBUG)

        self.watchdog.is_started = True

        if Global.CONST.TEST:
            self.logger.log('Start TestRunner...', __name__, Logger.SEVERITY_DEBUG)
            self.test_runner.start()
            time.sleep(1)
            self.logger.log('TestRunner started', __name__, Logger.SEVERITY_DEBUG)

    @staticmethod
    def init_dirs():
        """Erstellt notwendige Ordner."""
        if not os.path.exists(Global.CONST.USER_DATA_DIR):
            os.makedirs(Global.CONST.USER_DATA_DIR)
        if not os.path.exists(Global.CONST.STH.DIR):
            os.makedirs(Global.CONST.STH.DIR)

    def run(self):
        """Startet die Applikation.
        
        :return: Rückgabewert der Applikation.
        """
        self.setFont(QtGui.QFont("DejaVu Sans", 20))
        return self.exec_()


if __name__ == '__main__':
    App = MediaDisplayServerApp(sys.argv)
    exit_code = App.run()
    if App.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN:
        App.os_interface.shutdown()
    elif App.state_machine.shutdown_mode == Global.CONST.SM.REBOOT:
        App.os_interface.reboot()
    else:
        App.os_interface.stop_service()
    sys.exit(exit_code)
