# -*- coding: utf-8 -*-
"""Dieses Modul stellt einige globale Instanzen zur Verfügung. Es darf nie ein reload(Global) ausgeführt werden!
"""

import json
import inspect
import os


class CONST(object):
    """Diese statische Klasse enthält einige Konstanten."""
    # Die Versionsnummer. Wird vom Buildtool gesetzt.
    VERSION = '+++ replace release +++'
    # Die Buildnummer. Wird von dem Buildtool gesetzt.
    BUILD = '+++ replace build +++'
    # Die ID des Commits. Wird von dem Buildtool gesetzt.
    COMMIT = '+++ replace commit +++'
    # Lizenz
    LICENSE = u'''Apache License, Version 2.0'''
    # Der Ordnerpfad der aktuellen Datei.
    SCRIPT_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    # Der Pfad zum user_data Ordner.
    USER_DATA_DIR = os.path.join(SCRIPT_DIR, '..', 'user_data')
    # Der Pfad zum book Ordner in user_data. Wird für Plan D benötigt
    BOOK_DIR = os.path.join(USER_DATA_DIR, 'books')
    # Der Pfad zum data Ordner.
    DATA_DIR = os.path.join(SCRIPT_DIR, 'data')
    # Der Pfad zum Ordner mit den Schriftarten.
    FONTS_DIR = os.path.join(DATA_DIR, 'Fonts')
    # Gibt an ob das Programm auf einem Raspberry ausgeführt wird
    ON_RPI = False
    try:
        if os.uname()[4].startswith("arm"):
            ON_RPI = True
        else:
            ON_RPI = False
    except AttributeError:
        pass
    # Gibt an ob sich das Programm im Testmodus befindet.
    TEST = os.path.exists('TestExecutionQueue.txt')

    class OSI(object):
        """Konstanten für das OsInterface"""
        # Pfad zum config file für lirc
        LIRC_CONFIG_FILE_PATH = os.path.join(os.path.dirname(os.path.abspath(
            inspect.getfile(inspect.currentframe()))), u'data/lircrc')
        # GPIO Port des roten Kanals der RGB-LED
        RED_LED = 19
        # GPIO Port des grünen Kanals der RGB-LED
        GREEN_LED = 21
        # GPIO Port des blauen Kanals der RGB-LED
        BLUE_LED = 23
        # Wartezeit der Updateschleife für die RGB-LED
        TIME_INCREMENT_LED = 0.01
        # Wartezeit der Updateschleife des OsInterface Moduls.
        TIME_INCREMENT_UTIL = 1.0
        # Mögliche Adressen des BME280
        BME280_ADDRESS_1 = 0x76
        BME280_ADDRESS_2 = 0x77

        class COLORS(object):
            """Farben der RGB-LED"""
            RED = [100, 0, 0]
            GREEN = [0, 100, 0]
            BLUE = [0, 0, 100]
            WHITE = [100, 100, 100]
            BLACK = [0, 0, 0]
            ORANGE = [100, 50, 0]
            PURPLE = [100, 0, 100]
            CYAN = [0, 100, 100]
            YELLOW = [100, 100, 0]
            OFF = BLACK

        class LED_STRIP(object):
            """Konstanten für die LED-Zeile"""
            # Anzahl der LED's
            COUNT = 8  # Number of LEDs
            # GPIO Pin
            GPIO_PIN = 18
            # Modulationsfrequenz typisch 800kHz
            FREQ = 800000
            DMA = 5
            # Invert required when using inverting buffer
            INVERT = False
            # Leuchtstärke 0..255
            BRIGHTNESS = 17

            class COLOR(object):
                """Farben für die Neopixel für den Dirigenten"""
                BLACK = 0
                WHITE = 16777215
                RED = 65280
                YELLOW = 16776960
                GREEN = 16711680
                BLUE = 255

    class WS(object):
        """Konstanten für den WebServer"""
        PORT = 58080

    class DS(object):
        """Konstanten für den DisplayServer"""
        PORT = 48080
        DOCUMENT_ROOT = os.path.join(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))),
                                     'DS_DocumentRoot')
        STATIC_REQUESTS = {
            '': ['redirect.html', "text/html"],
            'index.html': ['index.html', "text/html"],
            'songs.html': ['songs.html', "text/html"],
            'lizenzen.html': ['lizenzen.html', "text/html"],
            'wetter.html': ['wetter.html', "text/html"],
            'slave.html': ['slave.html', "text/html"],
            'about.html': ['about.html', "text/html"],
            'favicon.ico': ['favicon.ico', "image/x-icon"],
            'static/Kurzanleitung.jpg': ['static/Kurzanleitung.jpg', 'image/jpg'],
            'static/style.css': ['static/style.css', 'text/css'],
            'static/slave.js': ['static/slave.js', 'application/javascript'],
            'static/songs.js': ['static/songs.js', 'application/javascript'],
            'static/index.js': ['static/index.js', 'application/javascript'],
            'static/weather.js': ['static/weather.js', 'application/javascript'],
            'static/sidebar.js': ['static/sidebar.js', 'application/javascript'],
            'robots.txt': ['robots.txt', 'text/plain'],
            'static/nav_logo.png': ['static/nav_logo.png', 'image/png'],
            'static/fonts/DejaVuSans.ttf': ['static/fonts/DejaVuSans.ttf', 'application/octet-stream'],
            'static/fonts/DejaVuSerif.ttf': ['static/fonts/DejaVuSerif.ttf', 'application/octet-stream'],
            'static/fonts/Roboto-Medium.ttf': ['static/fonts/Roboto-Medium.ttf', 'application/octet-stream'],
            'static/fonts/OPTICalculator.otf': ['static/fonts/OPTICalculator.otf', 'application/octet-stream'],
        }
        API_VERSION = '0.5'

    class CS(object):
        """Konstanten für den CommandServer"""
        PORT = 8080

    class AI(object):
        """Konstanten für das AjaxInterface"""
        # Fehlermeldungen
        ERROR0 = json.dumps({u'ok': False, u'errnr': 0, u'error': u'Invalid name or password.'})
        ERROR1 = json.dumps({u'ok': False, u'errnr': 1, u'error': u'User already logged in.'})
        ERROR2 = json.dumps({u'ok': False, u'errnr': 2, u'error': u'Not logged in.'})
        ERROR3 = json.dumps({u'ok': False, u'errnr': 3, u'error': u'No such command.'})
        ERROR4 = json.dumps({u'ok': False, u'errnr': 4, u'error': u'Book not found.'})
        ERROR5 = json.dumps({u'ok': False, u'errnr': 5, u'error': u'Song not found.'})
        ERROR6 = json.dumps({u'ok': False, u'errnr': 6, u'error': u'No song.'})

    class SH(object):
        """Konstanten für den SessionHandler"""
        SESSION_LIFETIME = 300.0  # in seconds

    class SM(object):
        """Konstanten für die StateMachine"""
        # Nummern der Zustände
        STATE_START = 0
        STATE_IDLE = 1
        STATE_SONG = 2
        STATE_NUMBER = 3
        STATE_BLUESCREEN = 4
        STATE_STANDBY = 5
        STATE_QUIT = 6
        STATE_CONFIRM_QUIT = 7
        STATE_SET_TIME = 8
        STATE_SET_DATE = 9
        STATE_SET_CLOCK_IDLE = 10
        STATE_SET_CLOCK_SONG = 11
        STATE_SET_TIME_NUMBER_SONG = 12
        STATE_SET_TIME_SONG_IDLE = 13
        STATE_SET_TIME_IDLE_STANDBY = 14
        STATE_SET_LOG_LEVEL = 15
        STATE_SYSTEM_INFORMATION = 16
        STATE_IP = 17
        STATE_SET_DISPLAY_SERVER = 18
        STATE_SET_GENERAL_SONG_BOOKS = 19
        STATE_SET_ORDER_SOLO = 20
        STATE_CONFIRM_UPDATE = 21
        STATE_DISPLAY_MESSAGE = 22
        STATE_ON_AIR = 23
        STATE_DICT = {
            0: u'Start',
            1: u'Zeige Uhr',
            2: u'Zeige Lied',
            3: u'Zeige Zahl',
            4: u'Bluescreen',
            5: u'Standby',
            6: u'Beenden',
            7: u'Bestätige Beenden',
            8: u'Stelle Zeit',
            9: u'Stelle Datum',
            10: u'Stelle Uhranzeige Uhr',
            11: u'Stelle Uhranzeige Lied',
            12: u'Stelle Dauer Nummer\u2794Lied',
            13: u'Stelle Dauer Lied\u2794Uhr',
            14: u'Stelle Dauer Uhr\u2794Standby',
            15: u'Stelle LogLevel',
            16: u'Systeminformationen',
            17: u'Zeige Netzwerkadresse',
            18: u'Stelle DisplayServer',
            19: u'Stelle Allg. Liederbücher',
            20: u'Stelle Reihenfolge Solisten',
            21: u'Bestätige Update',
            22: u'Zeige Nachricht',
            23: u'On Air',
        }
        # Kommando-Typ System
        CMD_SYS = 0
        # Kommando-Typ Infrarot
        CMD_IR = 1
        # Kommando-Typ Netzwerk
        CMD_NET = 2

        # Aktion zeige Uhr
        ACTION_SHOW_CLOCK = 0
        # Aktion zeige Lied
        ACTION_SHOW_SONG = 1
        # Aktion zeige Lied nochmal
        ACTION_SHOW_SONG_AGAIN = 2
        # Aktion zeige Nachricht
        ACTION_SHOW_MESSAGE = 3
        # Aktion versetze in Standby
        ACTION_STANDBY = 4
        # Aktion Herunterfahren
        ACTION_SHUTDOWN = 5
        # Aktion Neustarten
        ACTION_REBOOT = 6

        class IRCMD(object):
            """Konstanten der Infrarot Kommandos"""
            N1 = '1'
            N2 = '2'
            N3 = '3'
            N4 = '4'
            N5 = '5'
            N6 = '6'
            N7 = '7'
            N8 = '8'
            N9 = '9'
            N0 = '0'
            N4_LIST = [N1,
                       N2,
                       N3,
                       N4
                       ]
            N_LIST = [N1,
                      N2,
                      N3,
                      N4,
                      N5,
                      N6,
                      N7,
                      N8,
                      N9,
                      N0
                      ]
            N1_9_LIST = [N1,
                         N2,
                         N3,
                         N4,
                         N5,
                         N6,
                         N7,
                         N8,
                         N9,
                         ]
            PLUS = '+'
            MINUS = '-'
            OK = 'ok'
            EXIT = 'exit'
            POWER = 'power'
            CONFIRM_STANDBY = '1'
            CONFIRM_SHUTDOWN = '2'
            CONFIRM_REBOOT = '3'
            CONFIRM_QUIT = '9'
            DELIMITER = '+'
            UP = 'up'
            DOWN = 'down'
            LEFT = 'left'
            RIGHT = 'right'
            BACK = 'back'
            MENU = 'menu'
            GUIDE = 'guide'
            MUTE = 'mute'
            VOLUME_UP = 'volumeup'
            VOLUME_DOWN = 'volumedown'
            AV = 'av'

        class TIME(object):
            """Zeitkonstanten"""
            # Bestätige Beenden -> Uhr
            CHANGE_7_1 = 30.0
            # Für den gesamten Einstellungsbereich
            CHANGE_SETTINGS = 30.0
            # Für Netzwerkadresse -> Uhr
            CHANGE_17_1 = 60.0
            # Min. Anzeigedauer Nachricht
            MIN_TIME_MESSAGE = 5
            # Default Anzeigedauer Nachricht
            DEFAULT_TIME_MESSAGE = 60
            # Max Anzeigedauer Nachricht
            MAX_TIME_MESSAGE = 600

        # Wartezeit der Updateschleife des StateMachine Moduls.
        TIME_INCREMENT = 0.01

        # Beenden-Modi
        NO_SHUTDOWN = 0
        SHUTDOWN = 1
        REBOOT = 2

    SCH = None

    class WD(object):
        """Konstanten für den WatchDog"""
        # Wartezeit der Updateschleife des WatchDog Moduls.
        TIME_INCREMENT = 5

    STH = None

    class SOH(object):
        """Konstanten für den SongHandler"""
        INVALID_BOOK = 'X'


class __SCH(object):
    """Konstanten für den ScreenHandler"""
    # Wartezeit der Updateschleife des ScreenHandler Moduls.
    TIME_INCREMENT = 0.001
    # Niedrigste Zahl für das Liederbuch
    MIN_BOOK = 0
    # Höchste Zahl für das Liederbuch
    MAX_BOOK = 99
    # Trennzeichen für Nummereingabe
    DELIMITER = ' '
    # Pfad zum QR-Code für die Fehlermeldung
    ERROR_QR_CODE_PATH = os.path.join(CONST.DATA_DIR, 'Images', 'ErrorQrCode.png')
    # Pfad zum richtigen QR-Code
    QR_CODE_PATH = os.path.join(CONST.USER_DATA_DIR, 'QrCode.png')

    class COLOR(object):
        """Konstanten für die Farben der GUI."""
        FG = u'#ffffff'
        BG = u'#000000'
        SETTINGS_FG = u'#00005a'
        SETTINGS_BG = u'#ffffff'
        SETTINGS_FG_OK = u'#2fb92f'
        SETTINGS_FG_EDIT = u'#237dff'
        ERROR_BG = u'#ff0000'
        SOLO_SOPRAN_FG = u'#000000'
        SOLO_SOPRAN_BG = u'#ff0000'
        SOLO_ALT_FG = u'#000000'
        SOLO_ALT_BG = u'#ffff00'
        SOLO_TENOR_FG = u'#000000'
        SOLO_TENOR_BG = u'#67bd5a'
        SOLO_BASS_FG = u'#ffffff'
        SOLO_BASS_BG = u'#0000ff'
        BOOK = u'#ffff00'
        BLUESCREEN = u'#0000ff'
        CLOCK_HOUR = u'#ffffff'
        CLOCK_MINUTE = u'#b4b4b4'
        CLOCK_STUDIO_FG = u'#ff0000'
        ON_AIR_FG = u'#ffffff'


CONST.SCH = __SCH


class __STH(object):
    """Konstanten für die Einstellungshandler"""
    # Einstellungsverzeichnis
    DIR = os.path.join(CONST.USER_DATA_DIR, 'settings')

    class GENERAL(object):
        """Konstanten für den GeneralSettingsHandler"""
        # Default Einstellungen
        DEFAULT = {'time_1_5': 60.0,  # Uhr -> Standby
                   'time_2_1': 60.0,  # Lied -> Uhr
                   'time_3_2': 5.0,  # Zahleingabe -> Lied
                   'clock_idle': '1',  # Uhr auf der Seite "Zeige Uhr"
                   'clock_song': '1',  # Uhr auf der Seite "Zeige Lied"
                   'display_server': '2',  # DisplayServer Ein/Aus,
                   'order_solo': '1',  # Reihenfolge der Solostimmen
                   'general_song_books': []  # Allgemeine Liederbücher
                   }
        TIME_1_5_MIN = 2.0
        TIME_1_5_MAX = 180.0
        TIME_2_1_MIN = 15.0
        TIME_2_1_MAX = 900.0
        TIME_3_2_MIN = 5.0
        TIME_3_2_MAX = 60.0
        CLOCK_IDLE_ANALOG = '1'
        CLOCK_IDLE_DIGITAL = '2'
        CLOCK_NO = '3'
        CLOCK_IDLE_LIVE_STUDIO = '9'
        CLOCK_IDLE_ALLOWED = (CLOCK_IDLE_ANALOG, CLOCK_IDLE_DIGITAL, CLOCK_NO, CLOCK_IDLE_LIVE_STUDIO)
        CLOCK_SONG_NO = '1'
        CLOCK_SONG_YES = '2'
        CLOCK_SONG_ALLOWED = (CLOCK_SONG_NO, CLOCK_SONG_YES)
        DISPLAY_SERVER_ON = '1'
        DISPLAY_SERVER_OFF = '2'
        DISPLAY_SERVER_ALLOWED = (DISPLAY_SERVER_ON, DISPLAY_SERVER_OFF)
        ORDER_SOLO_SIMPLE = '1'
        ORDER_SOLO_NORMAL = '2'
        ORDER_SOLO_MIRRORED = '3'
        ORDER_SOLO_TITLE = '4'
        ORDER_SOLO_ALLOWED = (ORDER_SOLO_SIMPLE, ORDER_SOLO_NORMAL, ORDER_SOLO_MIRRORED, ORDER_SOLO_TITLE)
        # Dateipfad
        FILEPATH = os.path.join(CONST.USER_DATA_DIR, 'settings', 'general.json')


CONST.STH = __STH
