# -*- coding: utf-8 -*-

from datetime import datetime as dt
import time
import os

import Global


REBOOT_HOUR = 3
REBOOT_MINUTE = 8
FILE_NAME = 'reboot_from_standby'


class RebootChecker(object):

    def __init__(self):
        self.__start_time = time.time()

    def check_reboot(self, seconds_since_last_command):
        now = dt.now()
        # do not reboot if last command is less than 1h ago
        if seconds_since_last_command < 60 * 60:
            return False
        # run for at least 24h - 10 min
        return (time.time() - self.__start_time) > (60 * 60 * 24 - 10 * 60) and \
            now.hour == REBOOT_HOUR and now.minute == REBOOT_MINUTE

    @staticmethod
    def __get_reboot_file_path():
        return os.path.join(Global.CONST.USER_DATA_DIR, FILE_NAME)

    def write_reboot_from_standby(self):
        open(self.__get_reboot_file_path(), 'w')

    def remove_reboot_from_standby(self):
        file_path = self.__get_reboot_file_path()
        if os.path.exists(file_path):
            try:
                os.remove(file_path)
            except IOError:
                pass

    def rebooted_from_standby(self):
        return os.path.exists(self.__get_reboot_file_path())
