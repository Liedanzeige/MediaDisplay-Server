# -*- coding: utf-8 -*-
"""Dieses Modul ist nur ein leeres Modul um das Betriebssystemverhalten zu simulierten, wenn die Anwendung nicht auf 
dem Raspberry ausgeführt wird. Für die richtige Dokumentation siehe OsInterfaceRaspberry.
"""

import time
import threading
import random
import socket

import Global
import DiagnosticModule


class OsInterface(object):

    def __init__(self, application):
        self.__application = application
        self.__thread = None
        self.__commands = list()
        self.__run = False

    def init_hardware(self):
        time.sleep(5)

    def init_led(self):
        time.sleep(20)

    def get_ir_commands(self):
        temp_commands = self.__commands[:]
        self.__commands = list()
        return temp_commands

    def add_ir_command(self, command):
        # Very important! This is a workaround for ir_large_input_test. For more information look for "Python GIL"
        open('TestExecutionQueue.txt', 'r')

        self.__commands.append(command)

    def set_led(self, color):
        pass

    def set_led_time(self, color, time):
        pass

    @staticmethod
    def hour_plus():
        pass

    @staticmethod
    def hour_minus():
        pass

    @staticmethod
    def minute_plus():
        pass

    @staticmethod
    def minute_minus():
        pass

    @staticmethod
    def set_date_time(time_str):
        pass

    @staticmethod
    def switch_hdmi_off():
        pass

    @staticmethod
    def switch_hdmi_on():
        pass

    @staticmethod
    def reboot():
        pass

    @staticmethod
    def shutdown():
        pass

    @property
    def serial_number(self):
        return u'Unter Windows nicht unterstützt'

    @property
    def temperature(self):
        return random.gauss(22.0, 1)

    @property
    def humidity(self):
        return random.gauss(34.0, 5)

    @property
    def pressure(self):
        return random.gauss(1010.0, 10)

    @staticmethod
    def stop_service():
        pass

    @staticmethod
    def get_ip():
        return socket.gethostbyname(socket.getfqdn())

    def is_running(self):
        if self.__thread is None:
            return False
        else:
            return self.__thread.is_alive()

    def start(self):
        if not self.is_running():
            self.__run = True
            self.__thread = threading.Thread(target=self.__loop, args=(), name='OsInterface')
            self.__thread.start()

    def stop(self):
        self.__run = False
        if self.__thread is not None:
            self.__thread.join(2 * Global.CONST.OSI.TIME_INCREMENT_LED)

    def __update(self):
        time.sleep(0.001)

    @DiagnosticModule.execute_with_exception_handling
    def __loop(self):
        while self.__run:
            self.__update()
            time.sleep(Global.CONST.OSI.TIME_INCREMENT_LED)
