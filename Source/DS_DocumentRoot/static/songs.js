function load_songs() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200){
                var data = JSON.parse(this.responseText);
                var text = '<table><tbody>\n';
                text += '<tr><th>Buch</th><th>Nummer</th><th>Titel</th><th>Datum</th></tr>\n';
                for (var i = 0; i < data.length; i++) {
                    text += '<tr><td>'
                    text += data[i].book_abbreviation
                    text += '</td><td>'
                    text += data[i].number
                    text += '</td><td>'
                    text += data[i].title
                    text += '</td><td>'
                    text += data[i].date.slice(8, 10)
                    text += '.'
                    text += data[i].date.slice(5, 7)
                    text += '.'
                    text += data[i].date.slice(0, 4)
                    text += ' '
                    text += data[i].date.slice(11, 19)
                    text += '</td></tr>\n'
                }
                text += '</tbody></table>\n'
                if (document.getElementById("sung-songs").innerHTML != text){
                    document.getElementById("sung-songs").innerHTML = text;
                };
                document.getElementById("message").innerHTML = '';
                document.getElementById("message").style.padding = '0em';
            }
            else {
                document.getElementById("message").innerHTML = 'Fehler bei Serverabfrage';
                document.getElementById("message").style.padding = '0.5em';
            }
        }
    };
    xhttp.open("GET", "sungsongs.json", true);
    xhttp.send();
 }

load_songs()
setInterval(load_songs, 1000);
