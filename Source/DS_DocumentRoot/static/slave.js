function load_number() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
            var number_element = document.getElementById("number");
            var parent_element = document.getElementById("number_parent");
            var header_element = document.getElementById("header");
            var footer_element = document.getElementById("footer");

            if (this.status == 200){
                data = JSON.parse(this.responseText);

                if (data.data){

                    if (data.data.book && data.data.color){
                        number_element.innerHTML = '<span style="color: ' + data.data.color + ';">' +
                        data.data.book.replace(' ', '&nbsp;') + '</span>&nbsp;' +
                        data.data.number.replace(' ', '&nbsp;');
                    }
                    else {
                        number_element.innerHTML = data.data.number;
                    }
                }
                else {
                    number_element.innerHTML = "";
                }
            }
            else{
                number_element.innerHTML = "Fehler";
            }

            number_element.style.display = "inline-block";
            number_element.style.fontSize = "1000px";
            number_element.style.textAlign = "center";
            number_element.style.position = "relative";
            number_element.style.padding = 0;
            number_element.style.margin = 0;
            parent_element.style.width = window.innerWidth + "px";
            var height = window.innerHeight - header_element.offsetHeight - 16 - footer_element.offsetHeight - 16;
            parent_element.style.height = height + "px";

            var widthFactor = window.innerWidth / number_element.offsetWidth;
            var heightFactor = height / number_element.offsetHeight;
            var scaleFactor = Math.min(widthFactor, heightFactor) * 1000;

            number_element.style.fontSize = Math.floor(scaleFactor) * 0.98 + "px";
            number_element.style.marginBottom = "0px";
            number_element.style.marginRight = "0px";

            number_element.style.marginTop = (height - number_element.offsetHeight) / 2 + "px";
            number_element.style.marginLeft = (window.innerWidth - number_element.offsetWidth) / 2 + "px";

            }

        };
    xhttp.open("GET", "slave.json", true);
    xhttp.send();
}

function toggle_color_scheme(){
    var elem = document.getElementById("body");
    if (elem.className === "black") {
        elem.className = "white";
        setCookie('color_scheme', 'white', 366);
    } else {
        elem.className = "black";
        setCookie('color_scheme', 'black', 366);
    }
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function load_color_scheme(){
    var scheme = getCookie('color_scheme');
    var elem = document.getElementById("body");
    if (scheme == 'white'){
        elem.className = "white";
        setCookie('color_scheme', 'white', 366);
    }
    else{
        elem.className = "black";
        setCookie('color_scheme', 'black', 366);
    }
}

load_number();
setInterval(load_number, 1000);
load_color_scheme();
