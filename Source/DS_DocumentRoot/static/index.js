function load_state() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200){
                data = JSON.parse(this.responseText);
                state_text = '';
                if (data.state_number == 2){
                    state_text += '<div class="song"><span class="number" style="color:' + data.data.book_color + '">' + data.data.book_abbreviation + '</span>'
                    state_text += '<span class="number"> ' + data.data.number + '</span></div><br>\n'
                    if (data.data.title){
                        state_text += '<span class="title">' + data.data.title + '<br></span>\n'
                    }
                    state_text += '<table><tbody>'
                    if (data.data.sopran == '1'){
                        state_text += '<tr><td id="solo-sopran">' + data.data.sopran_name + '</td></tr>\n'
                    }
                    else{
                        // state_text += '<span id="solo-no">' + data.data.sopran_name + '</span>'
                    }
                    if (data.data.alt == '1'){
                        state_text += '<tr><td id="solo-alt" style="">' + data.data.alt_name + '</td></tr>\n'
                    }
                    else{
                        // state_text += '<span id="solo-no">' + data.data.alt_name + '</span>'
                    }
                    if (data.data.tenor == '1'){
                        state_text += '<tr><td id="solo-tenor">' + data.data.tenor_name + '</td></tr>\n'
                    }
                    else{
                        // state_text += '<span id="solo-no">' + data.data.tenor_name + '</span>'
                    }
                    if (data.data.bass == '1'){
                        state_text += '<tr><td id="solo-bass">' + data.data.bass_name + '</td></tr>\n'
                    }
                    else{
                        // state_text += '<span id="solo-no">' + data.data.bass_name + '</span>'
                    }
                    state_text += '</tbody></table>'
                }
                else if (data.state_number == 3){
                    splitted_song_number = data.data.split(' ');
                    if (splitted_song_number.length < 2){
                        state_text = '<div class="song-number">' + data.data + '</div>\n';
                    }
                    else{
                        state_text = '<div class="song-number"><span style="color: #ffff00;">' + splitted_song_number[0] + '</span> ' + splitted_song_number.slice(1).join(' ') + '</div>\n';
                    }
                }
                text = '<h1>Aktueller Zustand: ' + data.state + '</h1>\n'  + state_text;
                if (document.getElementById("state").innerHTML != text){
                    document.getElementById("state").innerHTML = text;
                };
                document.getElementById("message").innerHTML = '';
                document.getElementById("message").style.padding = '0em';
            }
            else {
                document.getElementById("message").innerHTML = 'Fehler bei Serverabfrage';
                document.getElementById("message").style.padding = '0.5em';
            }
        }
    };
    xhttp.open("GET", "state.json", true);
    xhttp.send();
 }

load_state()
setInterval(load_state, 1000);
