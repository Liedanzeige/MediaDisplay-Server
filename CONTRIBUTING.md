# Contribution Guide

Python Code der eingecheckt wird:
 * muss dem Styleguide [PEP-8](https://www.python.org/dev/peps/pep-0008/) entsprechen. Dieses wird von PyCharm überprüft.
 * muss leicht zu Python 3.x konvertierbar sein. So dürfen z.B. eine Old-Style Classes verwendet werden. Siehe auch [hier](https://docs.python.org/3/howto/pyporting.html) und [hier](https://docs.python.org/3.0/library/2to3.html)
 * muss möglichst kein Module außerhalb der Standardbibliothek verwenden (wegen deren Lizenzen).
 * darf keinen auskommentierten Code haben, wenn er in den "master"-Branch eingecheckt wird.
 * darf keine TODO's haben, wenn er in den "master"-Branch eingecheckt wird.
 * muss UTF-8 kodiert sein (# -\*- coding: utf-8 -\*- in der ersten Zeile).
 * muss LF als Zeilenumbruch verwenden. Um PyCharm umzustellen siehe [hier](https://www.jetbrains.com/help/pycharm/2016.3/configuring-line-separators.html).
 * muss mit [Sphinx](http://www.sphinx-doc.org/en/stable/) dokumentiert sein.
